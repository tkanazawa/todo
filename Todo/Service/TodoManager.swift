//
//  TodoManager.swift
//  Todo
//
//  Created by 金澤 拓哉 on 2018/07/20.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import RealmSwift

class TodoManager: NSObject {
    
    func fetchTodoItems( result : (_ items : [TodoItem]) -> Void) {
        let realm = try! Realm()
        let items = realm.objects(TodoItem.self)
        
        var todoItems = [TodoItem]()
        for item in items {
            let _item = TodoItem()
            _item.id = item.id
            _item.title = item.title
            _item.limitDate = item.limitDate
            _item.color = item.color
            _item.other = item.other
            todoItems.append(_item)
        }
        result(todoItems)
    }
    
    func addTodoItem(item : TodoItem) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(item)
        }
    }
    
    func updateTodoItem(item : TodoItem) {
        let realm = try! Realm()
        guard let _item = realm.object(ofType: TodoItem.self, forPrimaryKey: item.id) else { return }
        try! realm.write() {
            _item.title = item.title
            _item.limitDate = item.limitDate
            _item.color = item.color
            _item.other = item.other
        }
    }
    
    func deleteTotoItem(item : TodoItem) {
        let realm = try! Realm()
        guard let _item = realm.object(ofType: TodoItem.self, forPrimaryKey: item.id) else { return }
        try! realm.write() {
            realm.delete(_item)
        }
    }
}
