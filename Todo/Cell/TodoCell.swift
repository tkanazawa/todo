//
//  TodoCell.swift
//  Todo
//
//  Created by 金澤 拓哉 on 2018/07/20.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import UIKit

class TodoCell: UITableViewCell {

    @IBOutlet weak var colorView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var limitDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
