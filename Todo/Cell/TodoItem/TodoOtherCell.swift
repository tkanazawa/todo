//
//  TodoOtherCell.swift
//  Todo
//
//  Created by 金沢拓哉 on 2018/07/21.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import UIKit

class TodoOtherCell: UITableViewCell {

    @IBOutlet weak var otherText: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        otherText.layer.borderWidth = 1
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
