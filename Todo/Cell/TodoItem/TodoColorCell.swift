//
//  TodoColorCell.swift
//  Todo
//
//  Created by 金沢拓哉 on 2018/07/21.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import UIKit

class TodoColorCell: UITableViewCell {

    @IBOutlet var buttonArray: [UIButton]!
    
    var selectedColor : TodoItem.Color = .none {
        didSet {
            buttonArray.forEach({
                $0.setImage( $0.tag == selectedColor.rawValue ? UIImage(named: "check") : nil, for: .normal)
            })
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        for (i,button) in buttonArray.enumerated() {
            button.tag = i
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func tapAction(_ sender: UIButton) {
        let color = TodoItem.Color.init(rawValue: sender.tag)
        selectedColor = color ?? .none
    }
}
