//
//  TodoLimitCell.swift
//  Todo
//
//  Created by 金沢拓哉 on 2018/07/21.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import UIKit

class TodoLimitCell: UITableViewCell {

    @IBOutlet weak var limitDaeField: UITextField!
    var toolBar:UIToolbar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        toolBar = UIToolbar()
        toolBar.sizeToFit()
        let toolBarBtn = UIBarButtonItem(title: "DONE", style: .plain, target: self, action: #selector(done))
        toolBar.items = [toolBarBtn]
        limitDaeField.inputAccessoryView = toolBar
    }

    @objc func done() {
        limitDaeField.resignFirstResponder()
    }
    
    @IBAction func textFieldTap(_ sender: UITextField) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "yyyy/MM/dd";
        limitDaeField.text = dateFormatter.string(from: sender.date)
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
