//
//  TodoTitleCell.swift
//  Todo
//
//  Created by 金沢拓哉 on 2018/07/21.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import UIKit

class TodoTitleCell: UITableViewCell {

    @IBOutlet weak var titleField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
