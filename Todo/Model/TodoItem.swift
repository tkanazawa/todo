//
//  TodoItem.swift
//  Todo
//
//  Created by 金澤 拓哉 on 2018/07/20.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import RealmSwift

class TodoItem: Object {
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var title = ""
    @objc dynamic var limitDate = ""
    @objc dynamic var other = ""
    @objc dynamic var color = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    enum Color : Int {
        case none
        case red
        case yellow
        case green
        case blue
    }
    
    func getUIColor() -> UIColor {
        guard let type : Color = Color(rawValue: color) else { return .clear }
        switch type {
        case .none: return .clear
        case .red: return .red
        case .yellow: return .yellow
        case .green: return .green
        case .blue: return .blue
        }
    }
}
