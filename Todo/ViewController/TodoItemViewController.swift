//
//  TodoItemViewController.swift
//  Todo
//
//  Created by 金澤 拓哉 on 2018/07/20.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import UIKit

class TodoItemViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    enum CellType : Int {
        case title
        case limit
        case color
        case other
        case submit
        
        func cellNames() -> String {
            switch self {
            case .title: return "TodoTitleCell"
            case .limit: return "TodoLimitCell"
            case .color: return "TodoColorCell"
            case .other: return "TodoOtherCell"
            case .submit: return "TodoSubmitCell"
            }
        }
    }
    
    init() {
        super.init(nibName: "TodoItemViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib.init(nibName: CellType.title.cellNames(), bundle: nil),forCellReuseIdentifier: CellType.title.cellNames())
        tableView.register(UINib.init(nibName: CellType.limit.cellNames(), bundle: nil),forCellReuseIdentifier: CellType.limit.cellNames())
        tableView.register(UINib.init(nibName: CellType.color.cellNames(), bundle: nil),forCellReuseIdentifier: CellType.color.cellNames())
        tableView.register(UINib.init(nibName: CellType.other.cellNames(), bundle: nil),forCellReuseIdentifier: CellType.other.cellNames())
        tableView.register(UINib.init(nibName: CellType.submit.cellNames(), bundle: nil),forCellReuseIdentifier: CellType.submit.cellNames())
        
        tableView.keyboardDismissMode = .onDrag
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getTitleCell(indexPath : IndexPath) -> TodoTitleCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.title.cellNames(), for: indexPath) as? TodoTitleCell else { return TodoTitleCell() }
        return cell
    }
    
    func getLimitCell(indexPath : IndexPath) -> TodoLimitCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.limit.cellNames(), for: indexPath) as? TodoLimitCell else { return TodoLimitCell() }
        return cell
    }
    
    func getColorCell(indexPath : IndexPath) -> TodoColorCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.color.cellNames(), for: indexPath) as? TodoColorCell else { return TodoColorCell() }
        return cell
    }
    
    func getOtherCell(indexPath : IndexPath) -> TodoOtherCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.other.cellNames(), for: indexPath) as? TodoOtherCell else { return TodoOtherCell() }
        return cell
    }
    
    func getSubmitCell(indexPath : IndexPath) -> TodoSubmitCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellType.submit.cellNames(), for: indexPath) as? TodoSubmitCell else { return TodoSubmitCell() }
        return cell
    }
    
}

extension TodoItemViewController : UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CellType.submit.rawValue + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type : CellType = CellType(rawValue: indexPath.row)!
        switch type {
        case .title: return getTitleCell(indexPath: indexPath)
        case .limit: return getLimitCell(indexPath: indexPath)
        case .color: return getColorCell(indexPath: indexPath)
        case .other: return getOtherCell(indexPath: indexPath)
        case .submit: return getSubmitCell(indexPath: indexPath)
        }
    }
}

extension TodoItemViewController : UITableViewDataSource {

}
