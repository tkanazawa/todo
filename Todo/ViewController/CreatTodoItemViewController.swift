//
//  CreatTodoItemViewController.swift
//  Todo
//
//  Created by 金澤 拓哉 on 2018/07/20.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import UIKit

class CreatTodoItemViewController: TodoItemViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "入力画面"
        let barButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(close))
        navigationItem.setLeftBarButton(barButton, animated: false)
    }
    
    @objc private func close() {
        dismiss(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func getLimitCell(indexPath: IndexPath) -> TodoLimitCell {
        let cell = super.getLimitCell(indexPath: indexPath)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat  = "yyyy/MM/dd";
        cell.limitDaeField.text = dateFormatter.string(from: Date())
        return cell
    }
    
    override func getSubmitCell(indexPath : IndexPath) -> TodoSubmitCell {
        let cell = super.getSubmitCell(indexPath: indexPath)
        cell.submitButton.addTarget(self, action: #selector(create), for: .touchUpInside)
        cell.submitButton.setTitle("作成", for: .normal)
        return cell
    }
    
    @objc func create() {
        let item = TodoItem()
        item.title = (tableView.cellForRow(at: IndexPath(row: CellType.title.rawValue, section: 0)) as! TodoTitleCell).titleField.text ?? ""
        item.limitDate = (tableView.cellForRow(at: IndexPath(row: CellType.limit.rawValue, section: 0)) as! TodoLimitCell).limitDaeField.text ?? ""
        item.color = (tableView.cellForRow(at: IndexPath(row: CellType.color.rawValue, section: 0)) as! TodoColorCell).selectedColor.rawValue
        item.other = (tableView.cellForRow(at: IndexPath(row: CellType.other.rawValue, section: 0)) as! TodoOtherCell).otherText.text ?? ""
        TodoManager().addTodoItem(item : item)
        close()
    }

}
