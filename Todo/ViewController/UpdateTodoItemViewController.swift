//
//  UpdateTodoItemViewController.swift
//  Todo
//
//  Created by 金澤 拓哉 on 2018/07/20.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import UIKit

class UpdateTodoItemViewController: TodoItemViewController {

    let item : TodoItem
    
    init(item : TodoItem) {
        self.item = item
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "編集画面"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func getTitleCell(indexPath : IndexPath) -> TodoTitleCell {
        let cell = super.getTitleCell(indexPath: indexPath)
        cell.titleField.text = item.title
        return cell
    }
    
    override func getLimitCell(indexPath : IndexPath) -> TodoLimitCell {
        let cell = super.getLimitCell(indexPath: indexPath)
        cell.limitDaeField.text = item.limitDate
        return cell
    }
    
    override func getColorCell(indexPath : IndexPath) -> TodoColorCell {
        let cell = super.getColorCell(indexPath: indexPath)
        let color = TodoItem.Color(rawValue: item.color)
        cell.selectedColor = color ?? .none
        return cell
    }
    
    override func getOtherCell(indexPath : IndexPath) -> TodoOtherCell {
        let cell = super.getOtherCell(indexPath: indexPath)
        cell.otherText.text = item.other
        return cell
    }
    
    override func getSubmitCell(indexPath : IndexPath) -> TodoSubmitCell {
        let cell = super.getSubmitCell(indexPath: indexPath)
        cell.submitButton.addTarget(self, action: #selector(update), for: .touchUpInside)
        cell.submitButton.setTitle("更新", for: .normal)
        return cell
    }
    
    @objc func update() {
        item.title = (tableView.cellForRow(at: IndexPath(row: CellType.title.rawValue, section: 0)) as! TodoTitleCell).titleField.text ?? ""
        item.limitDate = (tableView.cellForRow(at: IndexPath(row: CellType.limit.rawValue, section: 0)) as! TodoLimitCell).limitDaeField.text ?? ""
        item.color = (tableView.cellForRow(at: IndexPath(row: CellType.color.rawValue, section: 0)) as! TodoColorCell).selectedColor.rawValue
        item.other = (tableView.cellForRow(at: IndexPath(row: CellType.other.rawValue, section: 0)) as! TodoOtherCell).otherText.text ?? ""
        TodoManager().updateTodoItem(item: item)
        navigationController?.popViewController(animated: true)
    }
}
