//
//  ViewController.swift
//  Todo
//
//  Created by 金澤 拓哉 on 2018/07/20.
//  Copyright © 2018年 tkanazawa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var items : [TodoItem] = []
    let todoManager = TodoManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "TODOリスト"
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib.init(nibName: "TodoCell", bundle: nil), forCellReuseIdentifier: "TodoCell")
        
        todoManager.fetchTodoItems(result: {
            items = $0
            tableView.reloadData()
        })
        
        let barButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTodo))
        navigationItem.setRightBarButton(barButton, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        todoManager.fetchTodoItems(result: {
            items = $0
            tableView.reloadData()
        })
    }
    
    @objc private func addTodo() {
        let controller = UINavigationController(rootViewController: CreatTodoItemViewController())
        present(controller, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension ViewController : UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        let controller = UpdateTodoItemViewController.init(item:item)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath) as? TodoCell else { return UITableViewCell() }
        let item = items[indexPath.row]
        cell.titleLabel.text = item.title
        cell.limitDateLabel.text = item.limitDate
        cell.colorView.backgroundColor = item.getUIColor()
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton: UITableViewRowAction = UITableViewRowAction(style: .normal, title: "完了") { (action, index) -> Void in
            self.todoManager.deleteTotoItem(item: self.items[indexPath.row])
            self.items.remove(at: index.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        deleteButton.backgroundColor = UIColor.red
        
        return [deleteButton]
    }
}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
